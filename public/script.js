
var it =0;
var stop = 0;
const listeli = document.querySelectorAll("#annee li");
const p = document.querySelector("#annee p");
const h2 = document.querySelector("#annee h2");
const ico = document.getElementById("info");
const pInfo = document.querySelector("#info + p");


const bt_switch = document.querySelectorAll("div.switch");

bt_switch.forEach(bt => {
    bt.addEventListener('click',switchSemestre);
})

function panoramaCompetences(x){
    switch(x){
        case 1:
            listeli[0].classList.add('active');
            p.textContent = "Développer des applications informatiques simples";
            h2.classList.remove('big');
            break;
        case 2: 
            listeli[0].classList.remove('active');
            listeli[1].classList.add('active');
            p.textContent = "Appréhender et construire des algorithmes";
            break;
        case 3: 
            listeli[1].classList.remove('active');
            listeli[2].classList.add('active');
            p.textContent = "Installer et configurer un poste de travail";
            break;
        case 4: 
            listeli[2].classList.remove('active');
            listeli[3].classList.add('active');
            p.textContent = "Concevoir et mettre en place une base de données à partir d'un cahier des charges client";
            break;
        case 5: 
            listeli[3].classList.remove('active');
            listeli[4].classList.add('active');
            p.textContent = "Identifier les besoins métiers des clients et des utilisateurs";
            break;
        case 6: 
            listeli[4].classList.remove('active');
            listeli[5].classList.add('active');
            p.textContent = "Identifier ses aptitudes pour travailler dans une équipe";
            break;
        default:
            listeli[5].classList.remove('active');
            p.textContent="";
            h2.classList.add('big');
            break;
        
    


    }
}

function selection(y){
    console.log("it"+it+"i"+y)
        if (it!=0){
            h2.classList.remove('big');
            listeli[it-1].classList.remove('active');
        }
        stop=2;
        it = y;
        panoramaCompetences(y);
}


ico.addEventListener('click',function(){
    pInfo.classList.toggle('nothidden');
})




listeli[0].addEventListener('click',function(){
    selection(1);
});
listeli[1].addEventListener('click',function(){selection(2);
});
listeli[2].addEventListener('click',function(){selection(3);
});
listeli[3].addEventListener('click',function(){selection(4);
});
listeli[4].addEventListener('click',function(){selection(5);
});
listeli[5].addEventListener('click',function(){selection(6);
});



setInterval(function() {
    if (stop<=0){
    it+=1;
    if (it>6){
        it=0;
    }
    panoramaCompetences(it);
    }else{
        stop-=1;
    }
},2000);


function switchSemestre(){
    window.scrollTo(top)
    document.querySelector("section.S1").classList.toggle('hidden');
    document.querySelector("section.S2").classList.toggle('hidden');

    bt_switch.forEach(bt => {
        bt.classList.toggle('hidden');
    })
}


const content = document.querySelectorAll('#listeSAE section');
function handleIntersection(entries) {
    entries.map((entry) => {
        if (entry.isIntersecting) {
            entry.target.classList.add('loaded')
            // Affiche le content visible dans la console
            console.log(entry.target);
            // observer.unobserve(entry.target);
        } else {
            entry.target.classList.remove('loaded')
        }
    });
}
const observer = new IntersectionObserver(handleIntersection);
content.forEach(image => observer.observe(image));




var it =0;
var stop = 0;
const listeli = document.querySelectorAll("#annee li");
const p = document.querySelector("#annee p");
const h2 = document.querySelector("#annee h2");
const ico = document.getElementById("info");
const pInfo = document.querySelector("#info + p");

function panoramaCompetences(x){
    switch(x){
        case 1:
            listeli[0].classList.add('active');
            p.textContent = "Adapter des applications sur un ensemble de supports (embarqué, web, mobile, IoT...)";
            h2.classList.remove('big');
            break;
        case 2: 
            listeli[0].classList.remove('active');
            listeli[1].classList.add('active');
            p.textContent = "Analyser et optimiser des applications";
            break;
        case 3: 
            listeli[1].classList.remove('active');
            listeli[2].classList.add('active');
            p.textContent = "";
            break;
        case 4: 
            listeli[2].classList.remove('active');
            listeli[3].classList.add('active');
            p.textContent = "";
            break;
        case 5: 
            listeli[3].classList.remove('active');
            listeli[4].classList.add('active');
            p.textContent = "";
            break;
        case 6: 
            listeli[4].classList.remove('active');
            listeli[5].classList.add('active');
            p.textContent = "Manager une équipe informatique";
            break;
        default:
            listeli[5].classList.remove('active');
            p.textContent="";
            h2.classList.add('big');
            break;
        
    


    }
}

function selection(y){
    console.log("it"+it+"i"+y)
        if (it!=0){
            h2.classList.remove('big');
            listeli[it-1].classList.remove('active');
        }
        stop=2;
        it = y;
        panoramaCompetences(y);
}


ico.addEventListener('click',function(){
    if (!pInfo.classList.toggle('hidden')){
        remplirP(pInfo,0,"Vous pouvez retrouvez sur cette page tous les SAE (Situation d'apprentissage en équipe) réalisé pendant ma première année de BUT."+ 
        "Il y a aussi mon auto evalution sur chacune des 6 compétences évaluer.");
        
    }
})

function remplirP(p,x,text){
    p.textContent = text.substring(0,x);
    if (x<text.length){
        setTimeout(function(){remplirP(p,x+1,text);},50);
    }
}


listeli[0].addEventListener('click',function(){
    selection(1);
});
listeli[1].addEventListener('click',function(){selection(2);
});
listeli[2].addEventListener('click',function(){selection(3);
});
listeli[3].addEventListener('click',function(){selection(4);
});
listeli[4].addEventListener('click',function(){selection(5);
});
listeli[5].addEventListener('click',function(){selection(6);
});



setInterval(function() {
    if (stop<=0){
    it+=1;
    if (it>6){
        it=0;
    }
    panoramaCompetences(it);
    }else{
        stop-=1;
    }
},2000);
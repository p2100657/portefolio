
let closefen = () => {
    document.querySelectorAll("#fenetre section").forEach((elmt) =>{elmt.classList.add("hidden")});
    document.querySelector("#fenetre h3").classList.remove("hidden");
    document.querySelectorAll("#ordinateur ol:first-child li").forEach((elmt)=>{elmt.classList.remove("selected")})

}

let openBd = () => {
    document.querySelectorAll("#fenetre section").forEach((elmt) =>{elmt.classList.add("hidden")});
    document.querySelector("#fenetre h3").classList.add("hidden");
    document.querySelector("#BD").classList.remove("hidden");
    let icons = document.querySelectorAll("#ordinateur ol:first-child li");
    icons.forEach((elmt)=>{elmt.classList.remove("selected")})
    icons[1].classList.add("selected")
}

let openReseau = () => {
    document.querySelectorAll("#fenetre section").forEach((elmt) =>{elmt.classList.add("hidden")});
    document.querySelector("#fenetre h3").classList.add("hidden");
    document.querySelector("#reseau").classList.remove("hidden");
    let icons = document.querySelectorAll("#ordinateur ol:first-child li");
    icons.forEach((elmt)=>{elmt.classList.remove("selected")})
    icons[2].classList.add("selected")
}

let openModel = () => {
    document.querySelectorAll("#fenetre section").forEach((elmt) =>{elmt.classList.add("hidden")});
    document.querySelector("#fenetre h3").classList.add("hidden");
    document.querySelector("#modelisation").classList.remove("hidden");
    let icons = document.querySelectorAll("#ordinateur ol:first-child li");
    icons.forEach((elmt)=>{elmt.classList.remove("selected")})
    icons[3].classList.add("selected")
}
let openOutil = () => {
    document.querySelectorAll("#fenetre section").forEach((elmt) =>{elmt.classList.add("hidden")});
    document.querySelector("#fenetre h3").classList.add("hidden");
    document.querySelector("#outils").classList.remove("hidden");
    let icons = document.querySelectorAll("#ordinateur ol:first-child li");
    icons.forEach((elmt)=>{elmt.classList.remove("selected")})
    icons[4].classList.add("selected")
}

let openProg = () =>{
    document.querySelectorAll("#fenetre section").forEach((elmt) =>{elmt.classList.add("hidden")});
    document.querySelector("#fenetre h3").classList.add("hidden");
    document.querySelector("#programmation").classList.remove("hidden");
    let icons = document.querySelectorAll("#ordinateur ol:first-child li");
    icons.forEach((elmt)=>{elmt.classList.remove("selected")})
    icons[0].classList.add("selected")
}

const curseur = document.getElementById("curseur");

let int1;
let int2;

const step1 = () =>{
    curseur.style.transform = "translate(10vmin,5vmin)";
    int1 = setTimeout(openProg,1200);
    int2 = setTimeout(step2, 2000);
}

const step2 = () =>{
    curseur.style.transform = "translate(20vmin,5vmin)";
    int1 = setTimeout(openBd,1200)
    int2= setTimeout(step3, 2000);
}

const step3 = () =>{
    curseur.style.transform = "translate(30vmin,5vmin)";
    int1 = setTimeout(openReseau,1200)
    int2=setTimeout(step4, 2000);
}

const step4 = () =>{
    curseur.style.transform = "translate(40vmin,5vmin)";
    int1 = setTimeout(openModel,1200)
    int2=setTimeout(step5, 2000);
}

const step5 = () =>{
    curseur.style.transform = "translate(50vmin,5vmin)";
    int1 = setTimeout(openOutil,1200)
    int2=setTimeout(step6, 2000);
}

const step6 = () =>{
    curseur.style.transform = "translate(117vmin,-48vmin)";
    int1 = setTimeout(closefen,1200)
    int2=setTimeout(step0, 2000);
}

const step0 = () =>{
    curseur.style.transform = "translate(100vmin,-25vmin)";
    int1 = setTimeout(closefen,1200)
    int2=setTimeout(step1, 2000);
}
            


const stopIntervals = () =>{
    clearTimeout(int1);
    clearTimeout(int2);
    curseur.style.display="none";
}     

const stopIntervals2 = () =>{
    clearTimeout(int1);
    clearTimeout(int2);
    closefen();
}  


function handleIntersection2(entries) {
    entries.map((entry) => {
        if (entry.isIntersecting) {
            stopIntervals2();
            step0();
        } 
    });
}

const intersectionObserver = new IntersectionObserver(handleIntersection2);
intersectionObserver.observe(document.querySelector("#competences"));

function handleIntersection(entries) {
    entries.map((entry) => {
        if (entry.isIntersecting) {
            entry.target.classList.add('loaded')
            console.log(entry.target);
        } else {
            entry.target.classList.remove('loaded')
        }
    });
}

const intersectionObserverCarte = new IntersectionObserver(handleIntersection);
intersectionObserverCarte.observe(document.querySelector("#profil"));
intersectionObserverCarte.observe(document.querySelector("#formations"));